require 'spec_helper'

describe 'test' do
    it { is_expected.to compile }
    it { is_expected.to contain_file('/root/test.txt').with(
        'ensure' => 'present',
        'content' => /World/
        ) }
    context 'on a node running CentOS' do
        let(:facts) do
            { os: 'CentOS' }
        end
        it { is_expected.to contain_notify('on CentOS') }
    end
    context 'on a node running Debian' do
        let(:facts) do
            { os: 'Debian' }
        end
        it { is_expected.to contain_notify('on Debian') }
    end
end
