# Introduction

#### Table of Contents

- [Commands](#commands)
- [Code](#code)
- [Notes](#notes)
- [Links](#links)

## Commands

``` sh
# For listing the sections in the presentation
ls -1
```

## Code

## Notes

Goal: Writing and running automated testing for Puppet code.

Hello, My Name is $name a member of $team

The entire presentation is available online at https://bitbucket.org/italy_portugal/lunch-n-learns
Feel free to follow along. 

This presentation is split into 6(7) sections: 
0. setting up a testing environment
1. checking syntax errors. 
2. linting code style
3. compiling the puppet catalog
4. validating VM configuration
5. generating test harnesses
6. creating short feedback loops for integrating testing into development

Let's start with setting up the environment (Ask about Ruby development knowledge during transition)

## Links

- [Puppet documentation](https://docs.puppet.com)
- [rspec-puppet](https://github.com/rodjek/rspec-puppet)
- [puppet-retrospec](https://github.com/nwops/puppet-retrospec)
- [beaker](https://github.com/puppetlabs/beaker)
- [serverspec](http://serverspec.org/)
- [beaker-rspec](https://github.com/puppetlabs/beaker-rspec)
- [lunch-n-learn repo](https://bitbucket.org/italy_portugal/lunch-n-learns)
