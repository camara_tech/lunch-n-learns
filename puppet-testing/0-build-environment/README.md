# Setting up the Environment

#### Table of Contents
1. [Commands](#commands)
1. [Notes](#notes)
1. [Links](#links)

## Commands

``` sh
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3; \
\curl -sSL https://get.rvm.io | bash -s stable; \
rvm install ruby; \
rvm use default

gem install bundler; \
touch Gemfile; \
echo "gem 'rake'" > Gemfile

touch Rakefile; \
echo "" >> Rakefile; \
echo "task :default do puts 'Hello World' end" > Rakefile

bundle install; bundle exec rake
```

## Notes

Goal: create a basic 'Hello World' setup for the environment

Who has worked with Ruby before? With RVM or rbenv? with bundler?
with rake? (Based on this may be able to skip to next section)


--- IF SKIPPING: This section would create a Gemfile that includes
rake and a Rakefile with a default task that outputs 'Hello World'

I control my version of ruby using RVM, so that I can test on
newer versions that may not yet be in the OS package repositories.

There are other systems for doing the same thing, such as rbenv

I control my gems using bundler, so that I have full control
of my dependencies

Adding a ruby dependencies is adding the ruby gem to the Gemfile,
so we add the rake gem

Rake stands for Ruby make, and the Rakefile functions like a
Makefile,

Here we are setting the default rake task to output "Hello World"

Build the environment and run the default rake task

End: We created a basic Ruby environment that we will build
upon in the following sections

## Links

- [RVM](rvm.io)
- [rbenv]
- [bundler]
- [rake]
