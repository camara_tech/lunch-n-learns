# Speeding up the Testing Feedback loop

#### Table of Contents
1. [Code](#code)
1. [Notes](#notes)
1. [Links](#links)

## Code

~/.bashrc
``` sh
function puppet-watch {
    local red=$( echo -e "\033[31m" )
    local green=$( echo -e "\033[32m" )
    local brown=$( echo -e "\033[33m" )
    local nocolor=$( echo -e "\033[0m" )
    echo "watching $(pwd)";
    fswatch -e ".*" -r -i "\\.rb$" . | xargs -I{} bundle exec rake spec SPEC={}
}

function puppet-watch2 {
    local red=$( echo -e "\033[31m" )
    local green=$( echo -e "\033[32m" )
    local brown=$( echo -e "\033[33m" )
    local nocolor=$( echo -e "\033[0m" )
    echo "watching $PWD"
    inotifywait -qrm --exclude 'spec/fixtures/*' --format %w%f -e close_write * | while read file
    do
        echo $file changed
        (echo $file | grep \.pp | xargs grep 'class') && ( echo $file | sed -e 's/manifests/spec\/classes/' -e 's/\.pp/_spec\.rb/') | xargs -I{} bundle exec rake spec SPEC={}
        (echo $file | grep \.pp | xargs grep 'define ') && ( echo $file | sed -e 's/manifests/spec\/defines/' -e 's/\.pp/_spec\.rb/') | xargs -I{} bundle exec rake spec SPEC={}
        (echo $file | grep _spec\.rb | grep acceptance ) && bundle exec rake beaker SPEC=${file}
        (echo $file | grep _spec\.rb | grep -v acceptance ) && bundle exec rake spec SPEC=${file}
done
```

## Notes

The whole point of doing this kind of testing is to create a 
confidence-boosting feedback loop, so I have some tools/tips 
that help speed up that loop so that we write tests and code 
more quickly.

1. fswatch/inotify (bash), guard (ruby)

2. editors (vim,Atom,emacs)
  - Linting and Validating: Syntastic (Vim), Flycheck (Emacs), Linter(Atom)
  - running rspec: vim-rspec (vim), rspec-mode(Emacs), rspec (Atom)
  - code snippets: ultisnips (vim), yasnippet (Emacs), rspec-snippets(Atom)
    (snippets are built-in to Atom)

## Links

- [fswatch](http://emcrisostomo.github.io/fswatch/)
- [inotify-tools](https://github.com/rvoicilas/inotify-tools/wiki)
- [guard](https://github.com/guard/guard)

__VIM__
- [Syntastic](http://vimawesome.com/plugin/syntastic)
- [vim-rspec](http://vimawesome.com/plugin/vim-rspec-red)
- [vim-snippets](http://vimawesome.com/plugin/vim-snippets)
- [ultisnips](http://vimawesome.com/plugin/ultisnips)

__EMACS__
- [flycheck](http://www.flycheck.org/en/latest/)
- [rspec-mode](https://www.emacswiki.org/emacs/RspecMode)
- [YASnippet](https://github.com/joaotavora/yasnippet)

__ATOM__
- [linter-puppet-lint](https://atom.io/packages/linter-puppet-lint)
- [linter-puppet](https://atom.io/packages/linter-puppet)
- [rspec](https://atom.io/packages/rspec)
- [rspec-snippets](https://atom.io/packages/rspec-snippets)
- [language-puppet](https://atom.io/packages/language-puppet)
- [language-ruby](https://atom.io/packages/language-ruby)
