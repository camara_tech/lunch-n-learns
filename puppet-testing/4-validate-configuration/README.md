# Applying Puppet Code

#### Table of Contents
1. [Commands](#commands)
1. [Code](#code)
1. [Notes](#notes)
1. [Links](#links)

## Commands
``` sh
bundle install; bundle exec rake; \
bundle exec rake syntax; \
bundle exec rake lint; \
bundle exec rake spec

echo "gem 'beaker'" >> Gemfile; \
echo "gem 'serverspec'" >> Gemfile; \
echo "gem 'beaker-rspec'" >> Gemfile

mkdir -p spec/acceptance/nodesets; \
touch spec/acceptance/nodesets/default.yml; \
vim spec/acceptance/nodesets/default.yml

touch spec/spec_helper_acceptance.rb; \
vim spec/spec_helper_acceptance.rb

mkdir -p spec/acceptance/classes; \
touch spec/acceptance/classes/test_spec.rb; \
vim spec/acceptance/classes/test_spec.rb

bundle install; bundle exec rake beaker

vim spec/classes/test_spec.rb; \
bundle exec rake spec

vim manifests/init.pp; \
bundle exec rake spec

bundle exec rake beaker

vim spec/acceptance/classes/test_spec.rb; \
bundle exec rake beaker

### Only if we have time! ###
touch spec/acceptance/nodesets/docker.yml; \
vim spec/acceptances/nodesets/docker.yml; \
bundle exec rake beaker BEAKER_set=docker

touch spec/acceptance/nodesets/openstack.yml; \
vim spec/acceptance/nodesets/openstack.yml; \
bundle exec rake beaker BEAKER_set=openstack

```

## Code

spec/acceptance/nodesets/default.yml
``` yml
HOSTS:
  centos-72-x64:
    roles:
      - master
    platform: el-7-x86_64
    box: puppetlabs/centos-7.2-64-nocm
    hypervisor: vagrant

```

spec/spec_helper_acceptance.rb
``` ruby
require 'beaker-rspec'

install_puppet_on(hosts, :default_action => 'gem_install')

RSpec.configure do |c|
  module_root = File.expand_path(File.join(File.dirname(__FILE__), '..'))
  c.before :suite do
    hosts.each do |host|
      copy_module_to(host, :source => module_root, :module_name => 'test', :target_module_path => '/etc/puppet/modules')
    end
  end
end
```

spec/acceptance/classes/test_spec.rb (V1)
``` ruby
require 'spec_helper_acceptance'

describe 'test class' do

  let(:manifest) { "class { 'test': }" }

  it 'should run without errors' do
    apply_manifest(manifest, :catch_failures => true)
  end
  it 'should run idempotently' do
    apply_manifest(manifest, :catch_changes => true)
  end

end
```

spec/acceptance/classes/test_spec.rb (v2)
``` ruby
require 'spec_helper_acceptance'

describe 'test class' do

  let(:manifest) { "class { 'test': }" }

  it 'should run idempotently without errors' do
    apply_manifest(manifest, :catch_failures => true)
    apply_manifest(manifest, :catch_changes => true)
  end

  describe file('/root/test.txt') do
    it { is_expected.to exist }
    it { is_expected.to be_file }

    its(:content) { is_expected.to match /Hello World/ }

  end
end
```

spec/acceptance/nodesets/openstack.yml
``` yml
HOSTS:
  centos-7-master:
    roles:
      - master
    platform: el-7-x86_64
    hypervisor: openstack
    image: 'CentOS 7 (x86_64)'
    flavor: m2.tiny
CONFIG:
  nfs_server: none
  consoleport: 443
  openstack_api_key: PASSWORD
  openstack_username: italy_portugal
  openstack_auth_url: http://openstack.jonathancamara.com:5000/v2.0/tokens
  openstack_tenant: italy_portugal
  openstack_network: 'italy_portugal initial network'
  openstack_keyname: italy_portugal_v1
```

spec/acceptance/nodesets/docker.yml
```yml
HOSTS:
  centos-7-x64:
    roles:
      - master
    platform: el-7-x86_64
    image: centos:7
    hypervisor: docker
    docker_preserve_image: true
    docker_cmd: '["usr/sbin/init"]'
    docker_image_commands:
        - 'yum install -y crontabs tar wget openssl sysvinit-tools iproute which initscripts'
CONFIG:
    type: foss
```

## Notes
Goal: validate the results from configuring the VM

Now that we can test the puppet catalog, let's test if applying
the catalog during a puppet run will accomplish what we expect.

Check that we are starting from the same point.

Note one of the tasks defined by puppetlabs_spec_helper is
`beaker`. This task spins up a machine that we specify, applies
the catalog and then runs tests on the final state of the machine.

In order to use this task we first need to add some gems. we
are adding three gems this time: beaker, beaker-rspec, and serverspec.

Next, we need to specify the machine on which the tests will
run. The beaker gem uses what is called a nodeset to specify
the machine. In this case, I'll be using the vagrant provider
with Centos 7 vagrant box. (create spec/acceptance/nodesets/default.yml)

Now, I will need to setup the machine before running the tests,
such as installing puppet and putting the module we are testing
onto the machine. The beaker-rspec gem helps us prep rspec as
part of the setup (create spec/spec_helper_acceptance.rb)      

The simplest test is applying a manifest that includes the class.
We are going to check if the manifest applies without error
and idempotently. (create spec/acceptance/classes/test_spec.rb).

Setup takes a while, and hopefully everything will pass. (It
won't, don't panic)

The notify resources are preventing the beaker test from being
idempotent. Let's modify our spec test, to not expect them,
remove them so that the tests pass. and then remove the tests
as they aren't necessary any more.

Rerun the beaker test and it should now pass.

With the matchers from the serverspec gem, let us check that
the file was created with the right content. (use v2 of test_spec.rb)

If we have time, demonstrate openstack and docker nodesets and
BEAKER_set, BEAKER_destroy, and BEAKER_provision Environment
variables

END: We have successfully tested that the puppet catalog will
accomplish what we expected it to do.

At this point I've covered the basics of puppet-testing, the
next two sections are aimed at going faster when creating,
validating, linting, and testing puppet code.

(as a side-note, as long as you have ssh access, the framework
provided by serverspec alone gives you the ability to test the
state of any running machine)

## Links

- [serverspec](http://serverspec.org/)
- [beaker github repository](https://github.com/puppetlabs/beaker)
- [Beaker rubydocs](http://www.rubydoc.info/github/puppetlabs/beaker/frames)
- [beaker-rspec](https://github.com/puppetlabs/beaker-rspec)
