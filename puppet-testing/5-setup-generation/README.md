# Generating Puppet Modules

#### Table of Contents
1. [Commands](#commands)
1. [Notes](#notes)
1. [Links](#links)

## Commands
``` sh
puppet module generate jcamara-test

cd test; \
bundle install; bundle exec rake test

PUPPET_VERSION='~> 3.5' bundle install; bundle exec rake test

echo "gem 'safe_yaml'" >> Gemfile; \
PUPPET_VERSION='~> 3.5' bundle install; bundle exec rake test

retrospec puppet -e; \ 
echo "gem 'beaker'" >> Gemfile; \
echo "gem 'serverspec'" >> Gemfile; \
echo "gem 'beaker-rspec'" >> Gemfile; \
sed -i "s/\(name => '\)/\1test/" spec/spec_helper_acceptance.rb; \
PUPPET_VERSION='~> 3.5' bundle update

bundle exec rake beaker

cd ..; \
retrospec puppet -e new_module --name test2

cd test2; \
sed -i "s/\(name => '\)/\1test2/" spec/spec_helper_acceptance.rb; \
echo "gem 'safe_yaml'" >> Gemfile; \
bundle install; bundle exec rake beaker
```

## Notes
Goal: demonstrate module generation and test harness generation 
tools for Puppet

'puppet module generate <namespace-name>' is part of the puppet 
gem and is the standard way of creating new modules for puppet.

Some of the files generated, like Gemfile and Rakefile, 
have more advanced configurations which allow the use of Environment 
Variables to change how the system works, such as the ability 
to set the puppet version which we are testing against.

puppet-retrospec was originally created to generate a testing 
framework around modules without it. It has expanded to be a 
way of templating how you want to setup your modules.

Point out that default templates are opinionated, and should 
be changed for our own use. Majority of the time I end up cleaning 
up the files we don't need.

## Links

[Puppet: Writing Modules](https://docs.puppet.com/puppet/latest/modules_fundamentals.html#writing-modules)

[puppet-retrospec](https://github.com/nwops/puppet-retrospec)

[retrospec-templates](https://github.com/nwops/retrospec-templates)

[Upcoming Puppet 3 deprecation Example: safe_yaml ](https://tickets.puppetlabs.com/browse/PUP-3796)
