## == Class: test
# Just an Example Class to test
class test {
    file {'/root/test.txt':
        ensure  => 'present',
        content => 'Hello World',
    }

    notify { "on ${::os}": }
}
